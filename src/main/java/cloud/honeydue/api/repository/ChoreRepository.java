package cloud.honeydue.api.repository;

import cloud.honeydue.api.model.Chore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChoreRepository extends JpaRepository<Chore, Long> {
         List<Chore> findByUserId(Long userId);
}
